### Terminal commands

    To run project: make all

### Viewing the app ###

    Open the following url on your browser to view swagger documentation
    http://127.0.0.1:5000/


### Using Postman ####

    Sample Postman script is under postman folder
