from flask import request
from flask_restplus import Resource

from app.main.util.decorator import token_required
from ..util.dto import UserDto
from ..service.user_service import save_new_user, get_current_user

api = UserDto.api
_user = UserDto.user


@api.route('/')
class UserList(Resource):
    @api.expect(_user, validate=True)
    @api.response(201, 'User successfully created.')
    @api.doc('create a new user')
    def post(self):
        data = request.json
        return save_new_user(data=data)


@api.route('/me')
@api.response(404, 'User not found.')
class User(Resource):
    @api.doc('get a user')
    @token_required
    @api.marshal_with(_user)
    def get(self):
        user = get_current_user(request)
        if not user:
            api.abort(404)
        else:
            return user
