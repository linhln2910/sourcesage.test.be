from flask import request
from flask_restplus import Resource

from app.main.service.auth_helper import Auth
from app.main.service.user_service import forgot_password, reset_password
from ..util.dto import AuthDto, UserDto

api = AuthDto.api
user_auth = AuthDto.user_auth
_user = UserDto.user
_email = AuthDto.forgot_password
_reset_password = AuthDto.reset_password


@api.route('/login')
class UserLogin(Resource):
    @api.doc('user login')
    @api.expect(user_auth, validate=True)
    def post(self):
        post_data = request.json
        return Auth.do_login(data=post_data)


@api.route('/forgot-password')
class ForgotPassword(Resource):
    @api.doc('forgot password')
    def post(self):
        data = request.json
        return forgot_password(data["email"])


@api.route('/reset-password')
class ResetPassword(Resource):
    @api.doc('reset password')
    def post(self):
        data = request.json
        return reset_password(data)
