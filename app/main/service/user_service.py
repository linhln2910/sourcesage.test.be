from app.main import db
from app.main.model.user import User
from app.main.service.auth_helper import Auth
from app.main.util.constant import CodeConstant
from .. import flask_bcrypt


def save_new_user(data):
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        new_user = User(
            email=data['email'],
            name=data['name'],
            age=data['age'],
            gender=data['gender'],
            password=data['password'],
            image=data.get("image")
        )

        save_changes(new_user)
        return generate_token(new_user)
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists. Please Log in.',
        }
        return response_object, 409


def get_a_user(user_id):
    return User.query.filter_by(id=user_id).first()


def get_current_user(request):
    res, status = Auth.get_logged_in_user(request)
    if res:
        return get_a_user(res.get('data').get('id'))
    else:
        response_object = {
            'status': 'fail',
            'message': 'Try again'
        }
        return response_object, CodeConstant.INTERNAL_SERVER_ERROR


def generate_token(user):
    try:
        auth_token = User.encode_auth_token(user.id)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
            'Authorization': auth_token.decode()
        }
        return response_object, 201
    except Exception as e:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again.'
        }
        return response_object, 401


def forgot_password(email):
    current_user = User.query.filter_by(email=email).first()
    if current_user:
        code = Auth.get_random_string()
        current_user.code = code
        db.session.commit()

        # Do send email to stub with reset password link

        response_object = {
            'status': 'success',
            'message': 'Successfully send email to reset password.',
            'code': code
        }
        return response_object, 200
    else:
        response_object = {
            'status': 'fail',
            'message': 'Email does not exist. Please try again.'
        }
        return response_object, 404


def reset_password(data):
    current_user = User.query.filter_by(code=data['code']).first()
    if current_user:
        current_user.password_hash = flask_bcrypt.generate_password_hash(data['new_password']).decode('utf-8')
        current_user.code = None
        db.session.commit()

        response_object = {
            'status': 'success',
            'message': 'Successfully set new password.',
        }
        return response_object, 200
    else:
        response_object = {
            'status': 'fail',
            'message': 'Invalid request. Please try again'
        }
        return response_object, 403


def save_changes(data):
    db.session.add(data)
    db.session.commit()
