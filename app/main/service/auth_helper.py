from app.main.model.user import User
from ..util.constant import CodeConstant, MessageConstant
import random


class Auth:

    @staticmethod
    def do_login(data):
        try:
            user = User.query.filter_by(email=data.get('email')).first()
            if user and user.check_password(data.get('password')):
                auth_token = User.encode_auth_token(user.id)
                if auth_token:
                    response_object = {
                        'status': 'success',
                        'message': 'Successfully logged in.',
                        'Authorization': auth_token.decode()
                    }
                    return response_object, CodeConstant.SUCCESS
            else:
                response_object = {
                    'status': 'fail',
                    'message': 'email or password does not match.'
                }
                return response_object, CodeConstant.UNAUTHENTICATED

        except Exception as e:
            print(e)
            response_object = {
                'status': 'fail',
                'message': 'Try again'
            }
            return response_object, CodeConstant.INTERNAL_SERVER_ERROR

    @staticmethod
    def get_logged_in_user(new_request):
        auth_token = new_request.headers.get('Authorization')
        if auth_token:
            resp = User.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                user = User.query.filter_by(id=resp).first()
                response_object = {
                    'status': 'success',
                    'data': {
                        'id': user.id,
                        'email': user.email,
                        'name': user.name,
                        'age': user.age,
                        'gender': user.gender,
                        'image': user.image
                    }
                }
                return response_object, CodeConstant.SUCCESS
            response_object = {
                'status': 'fail',
                'message': 'Try again'
            }
            return response_object, CodeConstant.UNAUTHENTICATED
        else:
            response_object = {
                'status': 'fail',
                'message': MessageConstant.INVALID_TOKEN
            }
            return response_object, CodeConstant.UNAUTHENTICATED

    @staticmethod
    def get_random_string(length=24, allowed_chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
        return ''.join(random.choice(allowed_chars) for i in range(length))
