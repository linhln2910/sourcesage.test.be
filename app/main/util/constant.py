class CodeConstant:
    SUCCESS = 200
    UNAUTHENTICATED = 401
    INTERNAL_SERVER_ERROR = 200


class MessageConstant:
    INVALID_TOKEN = 'Provide a valid auth token.'
