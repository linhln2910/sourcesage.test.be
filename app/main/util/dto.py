from flask_restplus import Namespace, fields


class UserDto:
    api = Namespace('user', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True, description='user email address'),
        'name': fields.String(required=True, description='user full name'),
        'age': fields.Integer(required=True, description='user age'),
        'gender': fields.Integer(required=True, description='user gender'),
        'password': fields.String(required=True, description='user password'),
        'image': fields.String(required=False, description='user image'),
    })


class AuthDto:
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The email address'),
        'password': fields.String(required=True, description='The user password '),
    })
    forgot_password = api.model('forgot_password', {
        'email': fields.String(required=True, description='The email address'),
    })
    reset_password = api.model('reset_password', {
        'code': fields.String(required=True, description='Reset code'),
        'new_password': fields.String(required=True, description='New password'),
    })
